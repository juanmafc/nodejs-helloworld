var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res) {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}

exports.bicicleta_create = function(req, res) {
    var body = req.body;
    var bici = new Bicicleta(body.id, body.color, body.modelo);    
    bici.ubicacion = [body.lat, body.lng];

    Bicicleta.add(bici);

    res.status(200).json({
        bicicleta: bici
    })
}

exports.bicicleta_delete = function(req, res) {
    Bicicleta.removeById(req.body.id)
    res.status(204).send();
}

exports.bicicleta_update = function(req, res) {    
    var requestId = req.params.id;
    try{
        var bici = Bicicleta.findById(requestId);
        var body = req.body;
        bici.color = body.color;
        bici.modelo = body.modelo;
        bici.ubicacion = [body.lat, body.lng];    
        res.status(204).send();
    } catch (error) {
        res.status(404).send(`Id:${requestId} not found`);
    }            
}