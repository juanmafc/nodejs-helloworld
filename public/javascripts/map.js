var coordinates = [-34.6012424, -58.3861497];
var zoom = 13;
var mymap = L.map('main_map').setView(coordinates, zoom);


L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoianVhbm1hZmMiLCJhIjoiY2tqNmpmZ3NsNjhhNzJ2cGRvdW1nZHIxMyJ9.pXyabPKs2bIGyRoIrHt5GA'
}).addTo(mymap);

$.ajax({
    dataType: 'json',
    url: 'api/bicicletas',
    success: function(result) {
        console.log(result);
        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
        })
    }
})