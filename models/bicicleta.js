class Bicicleta {

    static allBicis = [];

    static add(aBici) {
        Bicicleta.allBicis.push(aBici);
    }

    static findById(aBiciId) {
        var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId)
        if(aBici)
            return aBici
        else
            throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
    }

    static removeById(aBiciId) {        
        for(var i = 0; i < Bicicleta.allBicis.length; i++) {
            if(Bicicleta.allBicis[i].id == aBiciId) {
                Bicicleta.allBicis.splice(i, 1);
                break;
            }
        }
    }

    constructor(id, color, modelo, ubicacion) {
        this.id = id;
        this.color = color;
        this.modelo = modelo;
        this.ubicacion = ubicacion;
    }

    toString() {
        return 'id: ' + this.id + ' | color: ' + this.color;
    }
    
}

var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
var b = new Bicicleta(2, 'blanca', 'urbana', [-34.596932, -58.3808287]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;

